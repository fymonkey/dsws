## dsws

R Access to Datastream API

### Background

dsws provides R with access to data and calculations from [Refinitiv Datastream](https://www.refinitiv.com/en/products/datastream-macroeconomic-analysis). 

### Requirements

First, you will need dependencies `httr`, `xml2`, `xts`, and `zoo`.

```{.r}
install.packages(c("httr", "xml2", "xts", "zoo"))
```

### Examples

Here are a few simple examples.

```{.r}
library(dsws)
dsws_connect(username = "XXXX000", password = "XXX000")

# static request
dsws_static("IBM", "P")

# time series requiest
gdp <- dsws_timeseries(instrument = "USGDP...D", datatype = "ESMAG", start_date = as.Date("2000-01-01"), end_date = as.Date("2010-01-01"))
```

### Status

Fully functional on Linux, OS X and Windows.

### Authors

FyMonkey

### License

MIT

